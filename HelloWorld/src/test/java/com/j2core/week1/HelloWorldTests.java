package com.j2core.week1;

import org.junit.Assert;
import org.junit.Test;

/**
 * Unit tests for {@link HelloWorld} class
 *
 * @author maxgls
 */
public class HelloWorldTests {

    @Test
    public void sumTest(){
        long result = new HelloWorld().sum(2, 3);
        Assert.assertEquals(5, result);
    }
}