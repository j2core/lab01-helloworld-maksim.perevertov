package com.j2core.week1;

/**
 * Sample class for learning stuff
 *
 * @author maxgls
 */
public class HelloWorld {

    public static void main(String[] args){
        System.out.println("Hello!");
    }

    public long sum(int a, int b){
        return a + b;
    }
}